/api/moneygram/get-crn

Method: GET

query params: {
    "type": "sender"/"receiver" 
    "deliveryOptionCode" //брать со справочника условий выплат/отправки для MoneyGram
    "lastName"
    "firstName"
    "phone"
    "transferCurrency", //брать со справочника валюты MoneyGram
    "transferToCountry", //брать со справочника страны MoneyGram
    "transferToPointId" //брать со справочника условий выплат/отправки для MoneyGram
}

response: {
    "Account" ? 
    "AccountNickname" ?
    "CustomerReceiveNumber" 
    "ReceiverLastName" ? 
    "ReceiverName" ?
    "ReceiverPhone" ?
}

==============================================================================================

/api/moneygram/calc-fee

Method: GET

query params: {
    transferToCountry, //брать со справочника страны MoneyGram
    transferCurrency, //валюта получения перевода, брать со справочника валюты MoneyGram
    transferSendCurrency, //валюта отправки перевода, брать со справочника валюты MoneyGram
    deliveryOptionCode, //брать со справочника условий выплат/отправки для MoneyGram
    calcMode, //брать со справочника типов комиссий MoneyGram
    amount
}

response {
      "transferTotalFeeCurrency": "840",
      "transferSendAmount": "110.00",
      "transferTotalFeeAmount": "20.00",
      "transferTotalSendAmount": "130.00",
      "transferCurrency": "840",
      "transferExchangeRate": "1.000000",
      "transferAmount": "110.00",
      "additionalResponse":       {
         "sendAmountInfo":          {
            "sendAmount": "110.00",
            "sendCurrency": "USD",
            "totalSendFees": "20.00",
            "totalDiscountAmount": "0.00",
            "totalSendTaxes": "0.00",
            "totalAmountToCollect": "130.00",
            "detailSendAmounts":             [
                              {
                  "amountType": "mgiNonDiscountedSendFee",
                  "amount": "20.00",
                  "amountCurrency": "USD"
               },
                              {
                  "amountType": "totalNonDiscountedFees",
                  "amount": "20.00",
                  "amountCurrency": "USD"
               },
                              {
                  "amountType": "discountedMgiSendFee",
                  "amount": "20.00",
                  "amountCurrency": "USD"
               },
                              {
                  "amountType": "mgiSendTax",
                  "amount": "0.00",
                  "amountCurrency": "USD"
               },
                              {
                  "amountType": "totalMgiCollectedFeesAndTaxes",
                  "amount": "20.00",
                  "amountCurrency": "USD"
               },
                              {
                  "amountType": "totalAmountToMgi",
                  "amount": "130.00",
                  "amountCurrency": "USD"
               },
                              {
                  "amountType": "totalSendFeesAndTaxes",
                  "amount": "20.00",
                  "amountCurrency": "USD"
               },
                              {
                  "amountType": "totalNonMgiSendFeesAndTaxes",
                  "amount": "0.00",
                  "amountCurrency": "USD"
               },
                              {
                  "amountType": "nonMgiSendTax",
                  "amount": "0.00",
                  "amountCurrency": "USD"
               },
                              {
                  "amountType": "nonMgiSendFee",
                  "amount": "0.00",
                  "amountCurrency": "USD"
               }
            ]
         },
         "estimatedReceiveAmountInfo":          {
            "receiveAmount": "110.00",
            "receiveCurrency": "USD",
            "validCurrencyIndicator": "true",
            "payoutCurrency": "USD",
            "totalReceiveFees": "0.00",
            "totalReceiveTaxes": "0.00",
            "totalReceiveAmount": "110.00",
            "receiveFeesAreEstimated": "false",
            "receiveTaxesAreEstimated": "false",
            "detailEstimatedReceiveAmounts":             [
                              {
                  "amountType": "nonMgiReceiveFee",
                  "amount": "0.00",
                  "amountCurrency": "USD"
               },
                              {
                  "amountType": "nonMgiReceiveTax",
                  "amount": "0.00",
                  "amountCurrency": "USD"
               },
                              {
                  "amountType": "mgiReceiveFee",
                  "amount": "0.00",
                  "amountCurrency": "USD"
               },
                              {
                  "amountType": "mgiReceiveTax",
                  "amount": "0.00",
                  "amountCurrency": "USD"
               }
            ]
         }
      }
   }]
}

====================================================================================================

/api/moneygram/get-method-info

Method: GET

query params: {
    "methodType", // значения: SendRequest/PayoutRequest,
    "transferToCountry", //брать со справочника страны MoneyGram
    "transferCurrency", //брать со справочника валюты MoneyGram
    "transferSendCurrency", //брать со справочника валюты MoneyGram
    "deliveryOptionCode", //брать со справочника условий выплат/отправки для MoneyGram
    "transferAmount"
}

response: {
      "Description": "Request to send transfer",
      "RequestFields":       [
                  {
            "Name": "TransferSendRequestMode",
            "Caption": "Mode of execution of the method SendRequest - Save/Send (Send by default)",
            "Type": "String",
            "MaxLength": "4",
            "Enumerated": "false",
            "IsRequired": "false"
         },

........................
                  {
            "Name": "PCTerminalNumber",
            "Caption": "PC Terminal Number",
            "Type": "String",
            "MaxLength": "10",
            "Enumerated": "false",
            "IsRequired": "false"
         }
      ],
    "ResponseFields":       [
                  {
            "Name": "TransferSysId",
            "Caption": "Transfer ID in system",
            "Type": "Integer",
            "MaxLength": "0",
            "Enumerated": "false",
            "IsRequired": "false"
         },
.......................
                  {
            "Name": "AdditionalResponse",
            "Caption": "Additional Response From System",
            "Type": "String",
            "MaxLength": "2000",
            "Enumerated": "false",
            "IsRequired": "false"
         }
      ]
   }]
}


====================================================================================================

/api/moneygram/transfer

/api/moneygram/payout

/confirm-product-operation

